<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="org.semanticannotation.similarityfinder.SimilarityFinder"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<html>
    <head>
        <title>Semantic Annotation</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="./css/main.css">
    </head>
    <body>
        <h1>Semantic Annotation</h1>
        <h2>Step 3 - Choose file for annotation</h2>
        <%
            Integer paramNumber = Integer.parseInt(request.getParameter("paramNumber"));
//            out.println(paramNumber);
            
            Map<String, String> directories = new HashMap<String, String>();
            for(int i=0; i<paramNumber; i++){
                String className = "class" + i;
                String paramName = "param" + i;
                String dir = request.getParameter(paramName);
                String ontClass = request.getParameter(className);
//                out.println(dir + ontClass);
                if(!dir.isEmpty()){
                    directories.put(dir, ontClass);
                }
            }
//            out.println(directories.toString());
            SimilarityFinder sf = SimilarityFinder.getInstance();
            sf.init();
            
            out.println("<i>Files in the following directories were indexed:<br>");
            out.println("<font size=\"2\">");
            out.println("<table id=\"classToDirTable\" align=\"center\">");
            out.println("<tr><th>class</th><th>directory</th></tr>");
            for(String dir : directories.keySet()){
                sf.indexDocuments(dir, directories.get(dir));
                out.println("<tr><td>" + directories.get(dir) + "</td><td>" + dir + "</td></tr>");
            }
            out.println("</table>");
            out.println("</font></i>");
            sf.closeIndex();
            
        %>
        <br>
        <br>
        <br>
        
        <form action="step4.jsp" method="get">
            <input type="text" name="queryFile" placeholder="Specify path of queryFile" 
                                    value="/home/serafeim/Desktop/vp1.pdf"/>
            <input type="submit" value="next">
        
        </form>
    </body>
</html>
