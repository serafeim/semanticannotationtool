<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<html>
    <head>
        <title>Semantic Annotation</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="./css/main.css">
    </head>
    <body>
        <h1>Semantic Annotation</h1>
        <h2>Step 1 - Load Owl Ontology</h2>
        <form action="step2.jsp" method="get">
            <input type="text" name="owlFile" placeholder="Specify path of owl ontology file" 
                                    value="/home/serafeim/Desktop/ontologies/paperOnt.owl"/>
            <input type="submit" value="next">
        
        </form>
    </body>
</html>
