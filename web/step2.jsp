<%-- 
    Document   : step2
    Created on : Dec 30, 2014, 8:35:02 PM
    Author     : serafeim
--%>

<%@page import="java.util.Queue"%>
<%@page import="java.util.LinkedList"%>
<%@page import="com.hp.hpl.jena.ontology.OntClass"%>
<%@page import="java.util.List"%>
<%@page import="org.semanticannotation.owlparser.OwlParser"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<jsp:useBean id="owlparser" class="org.semanticannotation.owlparser.OwlParser" scope="session"/>
<jsp:setProperty name="owlparser" property="*"/>

<html>
    <head>
        <title>Semantic Annotation</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="./css/main.css">        
        <script>
            function showLoading() {
                document.getElementById("loading_div").style.display = 'block';
            }		

        </script>
    </head>
    <body>
        <h1>Semantic Annotation</h1>
        <h2>Step 2 - Feed ontology leaf classes</h2>
        <%
            String owlFile = request.getParameter("owlFile");
            out.println("<i><font size=\"2\">(File loaded: " + owlFile + ")</font></i><br><br>");
            OntClass root = owlparser.getOntRootClass(owlFile);

            
            
            List<OntClass> subClasses = owlparser.getOntSubclasses(root);
            if(subClasses != null){
                out.print(root.getLocalName() + " -> ");
                for(OntClass t : subClasses){
                    out.println(t.getLocalName() + " ");
                }
                out.println("<br>");
                
                Queue<OntClass> q = new LinkedList<OntClass>(subClasses);

                out.println("<form action=\"step3.jsp\" method=\"get\" id=\"submitForm\">");
                int paramNumber = 0;
                while(!q.isEmpty()){
                    
                    OntClass c = q.remove();
                    List<OntClass> s = owlparser.getOntSubclasses(c);

                    if(s != null){
                        q.addAll(s);
                        out.print(c.getLocalName() +  " -> ");
                        for(OntClass t : s){
                            out.println(t.getLocalName() + " ");
                        }
                        out.println("<br>");
                    } 
                    else{
                        String value = "";
                        if(c.getLocalName().equals("VisualPerceptionPaper")){
                            value = "/home/serafeim/Desktop/ontologies/input_files/CognitiveSciencePapers/VisualPerceptionPapers/";
                        }
                        else if(c.getLocalName().equals("SpeechPaper")){
                            value = "/home/serafeim/Desktop/ontologies/input_files/CognitiveSciencePapers/SpeechPapers/";
                        }
                        else if(c.getLocalName().equals("DistributedSystemsPaper")){
                            value = "/home/serafeim/Desktop/ontologies/input_files/ComputerSciencePapers/DistributedSystemsPapers/";
                        }
                        else if(c.getLocalName().equals("DatabasePaper")){
                            value = "/home/serafeim/Desktop/ontologies/input_files/ComputerSciencePapers/DatabasePapers/";
                        }
                        
                        out.print(c.getLocalName() + "<input type=\"text\" name=\"param" + paramNumber + "\""
                                + " placeholder=\"Specify local path for folder contatining pdf files\"" +
                                    "value=\""+ value +"\"/><br>");
                        out.println("<input type=\"hidden\" name=\"class" + paramNumber +"\" value=\"" + c.getLocalName() + "\"/>");
                        paramNumber++;
                    }

                }
                out.println("<input type=\"hidden\" name=\"paramNumber\" value=\"" + paramNumber + "\"/>");
                out.println("<input type=\"submit\" value=\"next\" onclick=\"showLoading();\"></form>");
            }
            
            
        %>
	<div id='loading_div'><img src='images/loading_grey.gif' alt="loading"/>
            <br> Wait until input files are indexed
	</div>
        

    </body>

</html>

