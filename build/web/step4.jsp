<%-- 
    Document   : step2
    Created on : Dec 30, 2014, 8:35:02 PM
    Author     : serafeim
--%>

<%@page import="java.util.Map"%>
<%@page import="java.io.File"%>
<%@page import="org.semanticannotation.similarityfinder.PdfParser"%>
<%@page import="org.semanticannotation.similarityfinder.SimilarityFinder"%>
<%@page import="java.util.LinkedList"%>
<%@page import="java.util.List"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<jsp:useBean id="owlparser" class="org.semanticannotation.owlparser.OwlParser" scope="session"/>
<jsp:setProperty name="owlparser" property="*"/>

<html>
    <head>
        <title>Semantic Annotation</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="./css/main.css">
    </head>
    <body>
        <h1>Semantic Annotation</h1>
        <h2>Step 4 - Results</h2>
        <%
            String queryFile = request.getParameter("queryFile");
            out.println("<i><font size=\"2\">(Query file path: " + queryFile + ")</font></i><br><br>");
            
            
            PdfParser parser = new PdfParser();
            String queryFileContent = parser.readContentFromDoc(new File(queryFile));
            
            SimilarityFinder sf = SimilarityFinder.getInstance();
            Map<String, Float> results = sf.getPossibleAnnotations(queryFileContent);
            out.println("<table id=\"classToDirTable\" align=\"center\">");
            out.println("<tr><th>class</th><th>percentage (%)</th></tr>");
            for(String s : results.keySet()){
                out.println("<tr><td>" + s + "</td><td>" + results.get(s) + "</td></tr>");
            }
            out.println("</table>");
            
        %>
    </body>
</html>
