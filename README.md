# README #

SemanticAnnotation/ folder is a Netbeans project

Source code can be found in SemanticAnnotation/src/ folder and web pages at SemanticAnnotation/web/

Source code is distributed under the GPLv3 license (see license.txt for details)

Executable war file can be found at /SemanticAnnotation/dist/ and can be deployed in Apache Tomcat Web Server.

Documentation of Java code in javadoc form can be seen opening Documentation/index.html