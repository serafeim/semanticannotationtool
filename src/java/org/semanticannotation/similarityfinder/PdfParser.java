/**
 * This file is part of the Automatic Semantic Annotation Tool
 * 
 * Automatic Semantic Annotation Tool is free software: you can redistribute
 * it and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License, 
 * or (at your option) any later version.
 * 
 * Automatic Semantic Annotation Tool is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Automatic Semantic Annotation Tool.  
 * If not, see <http://www.gnu.org/licenses/>.
 */

package org.semanticannotation.similarityfinder;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.pdfbox.exceptions.CryptographyException;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.util.PDFTextStripper;
import org.apache.pdfbox.util.PDFTextStripperByArea;

/**
 * A simple parser for pdf files
 * @author serafeim
 */
public class PdfParser {
    
    /**
     * Gets content from a pdf file
     * @param file the specified file
     * @return the content of the file as plain text
     */
    public String readContentFromDoc(File file){
        String content = "";
        try
        {
            PDDocument document = PDDocument.load(file);
            document.getClass();
            if( document.isEncrypted() )
            {
                document.decrypt( "" );
            }

            PDFTextStripperByArea stripper = new PDFTextStripperByArea();
            stripper.setSortByPosition( true );
            PDFTextStripper s = new PDFTextStripper();
            content = s.getText(document);
            document.close();
        } catch (CryptographyException | IOException ex) {
            Logger.getLogger(PdfParser.class.getName()).log(Level.SEVERE, null, ex);
        }
        return content;
    }
}
