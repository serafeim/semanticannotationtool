/**
 * This file is part of the Automatic Semantic Annotation Tool
 * 
 * Automatic Semantic Annotation Tool is free software: you can redistribute
 * it and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License, 
 * or (at your option) any later version.
 * 
 * Automatic Semantic Annotation Tool is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Automatic Semantic Annotation Tool.  
 * If not, see <http://www.gnu.org/licenses/>.
 */

package org.semanticannotation.similarityfinder;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.Field.Store;
import org.apache.lucene.document.FieldType;
import org.apache.lucene.document.StringField;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.IndexWriterConfig.OpenMode;
import org.apache.lucene.queries.mlt.MoreLikeThis;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.RAMDirectory;
import org.apache.lucene.util.Version;
import org.semanticannotation.electionalgorithm.KnnAlgorithm;

/**
 * Mechanism for finding similarities between documents
 * @author serafeim
 */
public class SimilarityFinder {
    private static SimilarityFinder instance = null;
    private Directory indexDir;
    private StandardAnalyzer analyzer;
    private IndexWriterConfig config;
    IndexWriter indexWriter;
    Integer docId = 1;
    Map<Integer, String> fileToClass = new HashMap<Integer, String>();

    private SimilarityFinder() { }

    /**
     * Singleton pattern
     * @return the instance of the object
     */
    public static synchronized SimilarityFinder getInstance() {
        if (instance == null) {
            instance = new SimilarityFinder();
        }
 
        return instance;
    }
 
    /**
     * Initialization takes place
     * @throws IOException 
     */
    public void init() throws IOException{
        analyzer = new StandardAnalyzer(Version.LUCENE_42);
        config = new IndexWriterConfig(Version.LUCENE_42, analyzer);
        config.setOpenMode(OpenMode.CREATE_OR_APPEND);

        //create index in memory
        indexDir = new RAMDirectory(); 
        
        indexWriter = new IndexWriter(indexDir, config);
        indexWriter.commit();
    }
 
    /**
     * Indexes documents of a folder
     * @param input_folder the folder
     * @param className the class/category name to be assigned to the documents of the folder
     * @throws IOException 
     */
    public void indexDocuments(String input_folder, String className) throws IOException{

        //find files in input dir
        File dir = new File(input_folder);
        PdfParser parser = new PdfParser();
        for (File file : dir.listFiles()){
            String content = parser.readContentFromDoc(file);
            Document doc = createDocument(docId.toString(), content);
            System.out.println(docId + " " + file);
            indexWriter.addDocument(doc);
            this.fileToClass.put(docId, className);
            docId++;
        }
    }
    
    /**
     * Closes index
     * @throws IOException 
     */
    public void closeIndex() throws IOException{
        indexWriter.commit();
        indexWriter.forceMerge(100, true);
        indexWriter.close();
        System.out.println(fileToClass);
    }
    
    /**
     * Creates a representation of a document in the index
     * @param id the id to be assigned to the document
     * @param content the content of the document
     * @return the representation of the document to be indexed
     */
    private Document createDocument(String id, String content) {
        FieldType type = new FieldType();
        type.setIndexed(true);
        type.setStored(true);
        type.setStoreTermVectors(true);     //keep term vectors

        Document doc = new Document();
        doc.add(new StringField("id", id, Store.YES));
        doc.add(new Field("content", content, type));
        return doc;
    }


    /**
     * Finds similar files and their score
     * @param searchForSimilar plain text of the query file to search similarities for
     * @return a list of similar files found and their similarity score
     * @throws IOException 
     */
    public List<FileSimilarity> findSimilar(String searchForSimilar) throws IOException {
        IndexReader reader = DirectoryReader.open(indexDir);
        IndexSearcher indexSearcher = new IndexSearcher(reader);

        MoreLikeThis mlt = new MoreLikeThis(reader);
        mlt.setMinTermFreq(0);
        mlt.setMinDocFreq(0);
        mlt.setFieldNames(new String[]{"content"});
        mlt.setAnalyzer(analyzer);


        StringReader sReader = new StringReader(searchForSimilar);
        Query query = mlt.like(sReader, null);

        TopDocs topDocs = indexSearcher.search(query, 10);
        ScoreDoc[] filterScoreDocsArray = topDocs.scoreDocs;
        
        List<FileSimilarity> fileSimilaritiesList = new ArrayList<FileSimilarity>();
        for (int i = 0; i < filterScoreDocsArray.length; ++i) {
            int docId = filterScoreDocsArray[i].doc;
            Document aSimilar = indexSearcher.doc(docId);
            Integer similarId = Integer.parseInt(aSimilar.get("id"));

            Float docScore = filterScoreDocsArray[i].score;
            
//            System.out.println("====similar finded====");
//            System.out.println("id:" + similarId);
//            System.out.println("score: " + docScore);

            fileSimilaritiesList.add(new FileSimilarity(similarId, docScore));
        }
        return fileSimilaritiesList;
    }
    
    /**
     * Gets possible annotation of a document
     * @param searchForSimilar the plain text of the query document
     * @return 
     * @throws IOException 
     */
    public Map<String, Float> getPossibleAnnotations(String searchForSimilar) throws IOException{
        List<FileSimilarity> fsList = findSimilar(searchForSimilar);
        KnnAlgorithm algorithm = new KnnAlgorithm();
        return sortByValue(algorithm.getPercentages(fsList, fileToClass));
    }
    
    public class FileSimilarity{
        
        public FileSimilarity(int _fileId, Float _score){
            this.fileId = _fileId;
            this.score = _score;
        }
        
        public int fileId;
        public Float score;
        
    }
    
    /**
     * Sorts a map based on the values
     * @param map gets an unsorted map 
     * @return a sorted by based on the values
     */
    static Map sortByValue(Map map) {
        List list = new LinkedList(map.entrySet());
        Collections.sort(list, new Comparator() {
             @Override
             public int compare(Object o1, Object o2) {
                  return ((Comparable) ((Map.Entry) (o2)).getValue())
                 .compareTo(((Map.Entry) (o1)).getValue());
             }
        });

        Map result = new LinkedHashMap();
        for (Iterator it = list.iterator(); it.hasNext();) {
            Map.Entry entry = (Map.Entry)it.next();
            result.put(entry.getKey(), entry.getValue());
        }
        return result;
    } 
}
