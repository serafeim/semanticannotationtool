/**
 * This file is part of the Automatic Semantic Annotation Tool
 * 
 * Automatic Semantic Annotation Tool is free software: you can redistribute
 * it and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License, 
 * or (at your option) any later version.
 * 
 * Automatic Semantic Annotation Tool is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Automatic Semantic Annotation Tool.  
 * If not, see <http://www.gnu.org/licenses/>.
 */

package org.semanticannotation.electionalgorithm;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.semanticannotation.similarityfinder.SimilarityFinder.FileSimilarity;

/**
 * Implementation of the Classification Algorithm k-NN
 * @author serafeim
 */
public class KnnAlgorithm {
    
    /**
     * Executes the weighted k-NN Algorithm's voiting
     * @param fsList list of the similar files and their similarity score
     * @param fileToClass map from document id to known category
     * @return 
     */
    public Map<String, Float> getPercentages(List<FileSimilarity> fsList, Map<Integer, String> fileToClass){
        int electionSize = fsList.size();
        Map<String, Float> result = new HashMap<String, Float>();
        
        
        //weighted voting
        for(int i=0; i<electionSize; i++){
            FileSimilarity fs = fsList.get(i);
            if(result.get(fileToClass.get(fs.fileId)) == null){
                result.put(fileToClass.get(fs.fileId), fs.score);
            }
            else{
                Float curScore = result.get(fileToClass.get(fs.fileId));
                curScore += fs.score;
                result.put(fileToClass.get(fs.fileId), curScore);
            }
        }
        
        //find percentages of results
        Float sum = (float) 0;
        for(String s : result.keySet()){
            Float f = result.get(s);
            sum += f;
        }
        
        Map<String, Float> percentages = new HashMap<>();
        for(String s : result.keySet()){
            percentages.put(s, result.get(s)*100/sum);
            
        }
        return percentages;
    }
}
