/**
 * This file is part of the Automatic Semantic Annotation Tool
 * 
 * Automatic Semantic Annotation Tool is free software: you can redistribute
 * it and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License, 
 * or (at your option) any later version.
 * 
 * Automatic Semantic Annotation Tool is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Automatic Semantic Annotation Tool.  
 * If not, see <http://www.gnu.org/licenses/>.
 */
package org.semanticannotation.owlparser;


import com.hp.hpl.jena.ontology.OntClass;
import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.ontology.OntModelSpec;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.util.FileManager;
import com.hp.hpl.jena.util.iterator.ExtendedIterator;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


/**
 * Parses OWL Ontology File
 * @author serafeim
 */
public class OwlParser {
    OntModel model = null;
    
    /**
     * Gets root class of the Ontology
     * @param owlFile OWL File specified
     * @return  the root of the OWL Ontology
     */
    public OntClass getOntRootClass(String owlFile){
        model = ModelFactory.createOntologyModel( OntModelSpec.OWL_MEM );
        
         //use FileManager to find the input file
        InputStream in = FileManager.get().open(owlFile);
        if (in == null) {
            throw new IllegalArgumentException("File: " + owlFile + " not found");
        } 
        model.read(in, "");
        ExtendedIterator<OntClass> it = model.listHierarchyRootClasses();
        
        OntClass root = null;
        if ( it.hasNext() ) {
            root = it.next();
        }
        return root;
    }
    
    /**
     * Gets ontology's subclasses for the specified class
     * @param parentClass the parent class
     * @return the subclasses of the parent class
     */
    public List<OntClass> getOntSubclasses(OntClass parentClass){
        List<OntClass> subclasses = null;
        
        if(parentClass.hasSubClass()){
            subclasses = new ArrayList<>();
            for (Iterator i = parentClass.listSubClasses(); i.hasNext();){
                subclasses.add((OntClass) i.next());
            }
        }
        return subclasses;
    }
}
